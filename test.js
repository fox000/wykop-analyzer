const test = require('ava');
const request = require('supertest');

const app = require('./app');

test('index is available for everyone', async t => {
    const res = await request(app).get('/');

    t.is(res.status, 200);
    t.deepEqual(res.body, {msg: 'YO!'});

});

test('user can log in', async t => {

    const agent = request.agent(app);

    let res = await agent.get('/');
    t.deepEqual(res.body, {Auth: 'NO'});

    res = await agent.post('/login')
        .type('form')
        .field('username', 'user')
        .field('password', 'pass');

    res = await agent.get('/');
    t.deepEqual(res.body, {Auth: 'YES'});

    console.log(res.body);


    // let cookie = res.header['set-cookie'][0];
    // t.truthy(cookie.indexOf('hey') > -1)

    // t.is(res.status, 200);
    // t.deepEqual(res.body, {msg: 'logged'});

});

test.only('user can log out', async t => {

    const agent = request.agent(app);

    await agent.post('/login')
        .type('form')
        .field('username', 'user')
        .field('password', 'pass');

    let res = await agent.get('/');
    t.deepEqual(res.body, {Auth: 'YES'});

    await agent.get('/logout');

    res = await agent.get('/');
    t.deepEqual(res.body, {Auth: 'NO'});


    // let cookie = res.header['set-cookie'][0];
    // t.truthy(cookie.indexOf('hey') > -1)

    // t.is(res.status, 200);
    // t.deepEqual(res.body, {msg: 'logged'});

});

// test('protected is available for logged in', async t => {
//     const res = await request(app).get('/protected');
//
//     t.is(res.status, 401);
//
// });