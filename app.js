const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const MockStrategy = require('passport-mock-strategy');


// var path = require('path');
// var cookieParser = require('cookie-parser');
// var logger = require('morgan');

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');

const app = express();

app.use(session({secret: "cats"}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(
    function (username, password, done) {

        if (username === 'user' && password === 'pass') {
            return done(null, {id: 1, name: 'Johnny'});
        }
        return done(null, false, {message: 'Incorrect username or password.'});
    }
));

passport.use(new MockStrategy({
    name: 'local',
}, (user, done) => {
    return done(null, {id: 1, name: 'Johnny'});
}));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    done(null, {id: 1, name: 'Johnny'});
});

// app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res, next) => {
    if (req.user) {
        console.log(req.user);
        return res.json({Auth: 'YES'});
    }
    return res.json({Auth: 'NO'});

});

app.get('/logout', (req, res, next) => {
    req.logout();
    res.redirect('/');
});

app.post('/login', passport.authenticate('local'), (req, res, next) => {
    // res.cookie('cookie', 'hey');
    if (req.user) {
        return res.send({msg: 'logged'});
    }
    return res.send({msg: 'not yet!'})
});

// app.use('/users', usersRouter);

module.exports = app;
